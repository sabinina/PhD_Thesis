
% Extract information about the diameter and axial separation of avaraged Nucleoporins
% Imput data: Reference Image after Single Particle Averaging (ref.em)
% Update: 2017-11-17
% Update: uses gaussian fit in order to ectract centroid position
% per each ring side (lateral) or Nucleoplasmic vs. cytoplasmic (axial)
% Author: VJS 
% Date: 2017-11-13

%% Read Reference

clear all;
PixSize = 7; % in nanometers
ref = tom_emread('TPR-SNAP_class3_8fold_ref_3.em');
ref = ref.Value; % 40x40x40 matrix
figure; tom_dspcub(ref); % look at the average image on gallery view

%% Avaraged Axial Distance Measurment

MaxProjX = sum(ref,1);
MaxProjX = reshape(MaxProjX,[],40);
figure ; imshow(MaxProjX, [0 5]);

v = sum(sum(ref,1),2); 
AxialProf = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf) ;
title('Particle Z Profile')


% recover the Index of the Max intensities (each ring) and comprare the distance 
% [MaxLess,IndexLess] = max(v(1:20)) ; %Identify Nuclear ring
% [MaxMore,IndexMore] = max(v(21:40)) ; %Identify Cytoplasmic ring
% IndexMore = 20 + IndexMore ; %Correct the Index
% Zdist = IndexMore-IndexLess; %
% ZdistInNano = Zdist * PixSize ; 
% disp(['Axial Distance from two detected rings is ', num2str(ZdistInNano), ' nm ' ]) ;
%figure ; plot(v) ;
%title(['SNAP-Nup107 / Particle Z Profile: ',num2str(ZdistInNano), ' nm between rings']);

%% Gaussian Fit with double gauss
% Centroid 1 = Nucleoplasmic Ring
Xaxis1 = [1:40]';
Yaxis1 = AxialProf(1:40);
figure; plot(Xaxis1,Yaxis1, 'o');
f1 = fit(Xaxis1,Yaxis1,'gauss2');
XCent1 = f1.b1;
XCent2 = f1.b2;
Zdist = (XCent1-XCent2)*PixSize ;
figure; plot(f1,Xaxis1,Yaxis1);
title(['Simulated / Particle Z Profile: ' , num2str(Zdist), ' nm between rings']);


%% Gaussian Fit for each ring (Nucl/Cyto) independently to extract Z dist
% Centroid 1 = Nucleoplasmic Ring
Xaxis1 = [1:17]';
Yaxis1 = AxialProf(1:17);
figure; plot(Xaxis1,Yaxis1, 'o');
f1 = fit(Xaxis1,Yaxis1,'gauss1');
XCent1 = f1.b1;
figure; plot(f1,Xaxis1,Yaxis1);
title(['SNAP-Seh1 / Centroid 1 position is: ' , num2str(XCent1), ' in Z']);

% Gaussian Fit for each ring side independently to extract diameter
% Centroid 2 = right position
Xaxis2 = [26:40]';
Yaxis2 = AxialProf(26:40);
figure; plot(Xaxis2,Yaxis2, 'o');
f2 = fit(Xaxis2,Yaxis2,'gauss1');
XCent2 = f2.b1 ;
figure; plot(f2,Xaxis2,Yaxis2);
title(['SNAP-Nup107 / Centroid 2 position is: ' , num2str(XCent2), ' in Z']);

%Substract the centroids to get the diameter of the ring
Xdist = XCent2 - XCent1 ;
Xdist = Xdist * PixSize; 
disp(['Axial Distance from detected ring is ', num2str(Xdist), ' nm ' ]) ;
figure; plot(f1,[1:40],AxialProf);
hold on
plot(f2,[1:40],AxialProf);
title(['Tpr-SNAP / Particle Z Profile: ' , num2str(Xdist), ' nm between rings']);
hold off


%% Avaraged Radial Distance Measurments

d = sum(ref,3); % axial sum-projection image 
%d= sum(ref(:,:,1:20),3);
figure ; imshow(d, [0 2]);

CentralLineProf = d(:,22);
%LineProf = sum(d,2);
figure ; plot(CentralLineProf) ;
%title('Particle X Profile')

% recover the Index of the Max intensities (ring border) and comprare the distance 
%[MaxLess2,IndexLess2] = max(CentralLineProf(1:20)) ; %Identify Nuclear ring
%[MaxMore2,IndexMore2] = max(CentralLineProf(21:40)) ; %Identify Cytoplasmic ring
%IndexMore2 = 20 + IndexMore2 ; %Correct the Index
%Xdist = IndexMore2-IndexLess2; %
%XdistInNano = Xdist * PixSize ; 

%% Gaussian Fit with double gauss
% Centroid 1 = Nucleoplasmic Ring
Xaxis1 = [1:40]';
Yaxis1 = CentralLineProf(1:40);
figure; plot(Xaxis1,Yaxis1, 'o');
f1 = fit(Xaxis1,Yaxis1,'gauss3');
%For FWHM we use c1*2.355
%f1.c1*2.355*PixSize
XCent1 = f1.b1;
XCent2 = f1.b2;
%XCent3 = f1.b3;
Xdist = (XCent1-XCent2)*PixSize ;
figure; plot(f1,Xaxis1,Yaxis1);
title(['Masked SNAP-Nup / Particle X Profile: ' , num2str(Xdist), ' nm between rings']);

%% Gaussian Fit for each ring side independently to extract diameter
% Centroid 1 = left position
Xaxis1 = [1:20]';
Yaxis1 = CentralLineProf(1:20);
figure; plot(Xaxis1,Yaxis1, 'o');
f1 = fit(Xaxis1,Yaxis1,'gauss1');
XCent1 = f1.b1;
figure; plot(f1,Xaxis1,Yaxis1);
title(['SNAP-Nup107 / Centroid 1 position is: ' , num2str(XCent1), ' in X']);

% Gaussian Fit for each ring side independently to extract diameter
% Centroid 2 = right position
Xaxis2 = [21:40]';
Yaxis2 = CentralLineProf(21:40);
figure; plot(Xaxis2,Yaxis2, 'o');
f2 = fit(Xaxis2,Yaxis2,'gauss1');
XCent2 = f2.b1 ;
figure; plot(f2,Xaxis2,Yaxis2);
title(['SNAP-Nup107 / Centroid 2 position is: ' , num2str(XCent2), ' in X']);

%Substract the centroids to get the diameter of the ring
Xdist = XCent2 - XCent1 ;
Xdist = Xdist * PixSize; 
disp(['Lateral Distance from detected ring is ', num2str(Xdist), ' nm ' ]) ;
figure; plot(f1,[1:40],CentralLineProf);
hold on
plot(f2,[1:40],CentralLineProf);
title(['Tpr-SNAP / Particle X Profile: ' , num2str(Xdist), ' nm in diameter']);
hold off
