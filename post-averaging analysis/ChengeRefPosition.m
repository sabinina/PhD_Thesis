% Author: VJS 
% Date: 2018-03-28
% Extract distances from two-color references / uses gaussian fit in order to ectract centroid position
% Re-positions Masked reference

%% Initialize

clear all;
PixSize = 7; % in nanometers

ref_NM = tom_emread('ref_Nup133/Two-color_Nup133_8fold_ref_2.em');
ref_NM = ref_NM.Value; % 40x40x40 matrix
figure; tom_dspcub(ref_NM); % look at the average image on gallery view

ref_Mask = tom_emread('ref_Nup133/Masked_Two-color_Nup133_nuc-cyt_8fold_ref_11.em');
ref_Mask = ref_Mask.Value; % 40x40x40 matrix
figure; tom_dspcub(ref_Mask); % look at the average image on gallery view
% MergedRef = ref_Ch1.*0.1 + ref_Ch2 ; % decrese ELYS signal by a constact
% figure; tom_dspcub(MergedRef); % look at the average image on gallery view

%% Avaraged Axial Distance Measurment
%% Averaged No Mask
MaxProjX = sum(ref_NM,1);
MaxProjX = reshape(MaxProjX,[],40);
figure ; imshow(MaxProjX, [0 2]);

v = sum(sum(ref_NM,1),2); 
AxialProf_NM = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf_NM) ;
title('Particle Z Profile: CH1')

% Double Gaussian Fit both rings (Nucl/Cyto) simultaneusly to extract Z dist

Xaxis1 = [1:40]';
Yaxis1 = AxialProf_NM(1:40);
figure; plot(Xaxis1,Yaxis1, 'o');
f1_NM = fit(Xaxis1,Yaxis1,'gauss2');
Zdist_NM = (f1_NM.b1 - f1_NM.b2) * PixSize ;
figure; plot(f1_NM,Xaxis1,Yaxis1);
title(['Nup position is: ' , num2str(Zdist_NM), ' in Z']);



%% Averaged with Mask
MaxProjX = sum(ref_Mask,1);
MaxProjX = reshape(MaxProjX,[],40);
figure ; imshow(MaxProjX, [0 2]);

v = sum(sum(ref_Mask,1),2); 
AxialProf_Mask = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf_Mask) ;
title('Particle Z Profile: CH2')

% Double Gaussian Fit both rings (Nucl/Cyto) simultaneusly to extract Z dist

Xaxis1 = [1:40]';
Yaxis1 = AxialProf_Mask(1:40);
figure; plot(Xaxis1,Yaxis1, 'o');
f1_Mask = fit(Xaxis1,Yaxis1,'gauss2');
Zdist_Mask = (f1_Mask.b1 - f1_Mask.b2) * PixSize ;
figure; plot(f1_Mask,Xaxis1,Yaxis1);
title(['Nup position is: ' , num2str(Zdist_Mask), ' in Z']);

%% Shift Signal Intensity by Centroid difference

Pos1 = f1_Mask.b1 - f1_NM.b1;
Pos2 = f1_NM.b2 - f1_Mask.b2 ;

NewPos = zeros(40,40,40);

for k = 1:24 % Shift down

    NewPos(:,:,k) = ref_Mask(:,:,k)*(1-Pos1) + ref_Mask(:,:,k+1)*Pos1;
        
end

NewPos(:,:,25) =  ref_Mask(:,:,25);
 
for k = 40:-1:26  % Shift up  
    
    NewPos(:,:,k) = ref_Mask(:,:,k)*(1-Pos2) + ref_Mask(:,:,k-1)*Pos2;
    
end

v = sum(sum(NewPos,1),2); 
AxialProf_NewPos = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf_NewPos, 'r') ;
hold on 
plot(AxialProf_Mask, 'g')
plot(AxialProf_NM, 'b')
title('Particle Z Profile: NewPos')
hold off

% Double Gaussian Fit both rings (Nucl/Cyto) simultaneusly to extract Z dist

Xaxis1 = [1:40]';
Yaxis1 = AxialProf_NewPos(1:40);
figure; plot(Xaxis1,Yaxis1, 'o');
f1_NewPos = fit(Xaxis1,Yaxis1,'gauss2');
Zdist_NewPos = (f1_NewPos.b1 - f1_NewPos.b2) * PixSize ;
figure; plot(f1_NewPos,Xaxis1,Yaxis1);
title(['Nup position is: ' , num2str(Zdist_NewPos), ' in Z']);

tom_dspcub(NewPos)
tom_emwrite('ref_Seh1/Masked_wo-color_Seh1_18-03-17_nuc-cyt_8fold_ref_11_NewPos.em',NewPos);


