% Adds independently averaged Nucleoplasmic and cytoplasmic Rings of the NPC
% Input: final averaged reference + Each Masked used for averaging
% Author: VJS 
% Date: 2018-02-23

%%
clear all;
ref_NR = tom_emread('ref_Nup107/Two-color_Nup107_18-03-16_nuc_8fold_ref_11.em');
ref_NR = ref_NR.Value;
mask_NR = tom_emread('2018-03-16_N107_nuc_mask.em');
mask_NR = mask_NR.Value;
%tom_dspcub(mask_NR)

ref_NR_masked = ref_NR.*mask_NR;

v = sum(sum(ref_NR_masked,1),2); 
AxialProf = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf) ;
title('Particle Z Profile')

tom_emwrite('ref_Nup107/Masked_Two-color_Nup107_18-03-16_nuc_8fold_ref_11.em',ref_NR_masked);

ref_CR = tom_emread('ref_Nup107/Two-color_Nup107_18-03-16_cyt_8fold_ref_11.em');
ref_CR = ref_CR.Value;
mask_CR = tom_emread('2018-03-16_N107_cyt_mask.em');
mask_CR = mask_CR.Value;
%tom_dspcub(mask_CR)

ref_CR_masked = ref_CR.*mask_CR;
%tom_dspcub(ref_CR_masked)

v = sum(sum(ref_CR_masked,1),2); 
AxialProf = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf) ;
title('Particle Z Profile')

tom_emwrite('ref_Nup107/Masked_Two-color_Nup107_18-03-16_cyt_8fold_ref_11.em',ref_CR_masked);

%%

ref_add = zeros(40,40,40);
%ref_NR = tom_emread('ref/Masked_Two-color_Nup133_nuc_8fold_ref_11.em');
%ref_NR = ref_NR.Value;
ref_NR = ref_NR_masked;

%ref_CR = tom_emread('ref/Masked_Two-color_Nup133_cyt_8fold_ref_11.em');
%ref_CR = ref_CR.Value;
ref_CR = ref_CR_masked;

ref_add = ref_NR + ref_CR;
tom_dspcub(ref_add)
tom_emwrite('ref_Nup107/Masked_Two-color_Nup107_18-03-16_nuc-cyt_8fold_ref.em',ref_add);

