
% Create Masks for both Nuclear and Cytoplasmic Rings independently with tom_ellipsemask
% Imput data: Reference Image before Single Particle Averaging (ref.em)
% Uses gaussian fit in order to extract Mask's centroid position
% Author: VJS 
% Date: 2018-02-21

%% READ REFERENCE

ref = emread('ref_Nup107/Two-color_Nup107_8fold_ref_2.em');

v = sum(sum(ref,1),2); 
AxialProf = v(:); % vector with total localizations per frame (Axial Profile)
figure ; plot(AxialProf) ; % Axial Profile
title('Particle Z Profile')


%% GAUSSIAN FIT OF NUC RING

% Gaussian Fit for each ring (Nucl/Cyto) independently to extract Z dist
% Centroid 1 = Nucleoplasmic Ring
Xaxis1 = [1:26]';
Yaxis1 = AxialProf(1:26);
figure; plot(Xaxis1,Yaxis1, 'o');
f1 = fit(Xaxis1,Yaxis1,'gauss1');
XCent1 = f1.b1;
figure; plot(f1,Xaxis1,Yaxis1);
title(['SNAP-Nup107 / Centroid 1 position is: ' , num2str(XCent1), ' in Z']);

vol=tom_ellipsemask(ones(40,40,40),13,13,3,0.5,[21 21 (f1.b1)]);
figure;tom_dspcub(vol)
figure;tom_dspcub(ref);
%vol=tom_spheremask(ones(40,40,40),f1.c1*2,1,[21 21 f1.b1]); 
tom_emwrite('2018-06-02_nuc_mask.em',vol)

%% GAUSSIAN FIT OF CYT RING

% Gaussian Fit for each ring side independently to extract diameter
% Centroid 2 = cytoplasmic ring
Xaxis2 = [28:40]';
Yaxis2 = AxialProf(28:40);
figure; plot(Xaxis2,Yaxis2, 'o');
f2 = fit(Xaxis2,Yaxis2,'gauss1');
XCent2 = f2.b1 ;
figure; plot(f2,Xaxis2,Yaxis2);
title(['SNAP-Seh1 / Centroid 2 position is: ' , num2str(XCent2), ' in Z']);

% Dist = XCent2-XCent1;
% Dist = Dist * 7;

vol2=tom_ellipsemask(ones(40,40,40),13,13,3,0.5,[21 21 (f2.b1)]);
tom_dspcub(vol2)
figure;tom_dspcub(ref);
%vol=tom_spheremask(ones(40,40,40),f1.c1*2,1,[21 21 f1.b1]); 
tom_emwrite('2018-06-02_cyt_mask.em',vol2)

