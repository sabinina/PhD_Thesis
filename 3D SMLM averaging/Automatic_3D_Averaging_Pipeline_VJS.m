%
% Current state of our 3D Single Particle Averaging
% TOM Toolbox needs to be installed
% written in Matlab-7.4
% 
% Required files in the path:
% create_list.m
% em_boxer.m
% reference.m
% taList2wedge.m
% storm_mask.em
% Updated by VJS

%% TIFF to EM Particles
% 1. Reads all .tiff particle images in each Dataset subfolder and creates a
% single Folder where all subsequently ordered EM particles are stored
% 2. Generates a Motive list for all the EM particles to be
% further averaged with the av3 package
% 3. Creates UniqueIdentifier.mat file which contains information about each
% particle's original file and number to trace back if needed [FileNumber
% OldID X Y Z NewID].
% VJS 05.02.17
% last update: VJS 11.07.17 

%% GENERATE EM Particles
% 1. Reads each subfolder with all extracted .tiff particle files and
% stores them subsequently orderes in .em format in a sigle folder (EMpar)

clear all;
mkdir EMpar;
FileNumArray = [];
% Define a starting folder.
start_path = 'extracted_pores/' ;
output_path = 'EMpar/' ;
% Get list of all subfolders.
allSubFolders = genpath(start_path);

% Parse into a cell array.
remain = allSubFolders;
listOfFolderNames = {};
while true
	[singleSubFolder, remain] = strtok(remain, ':');
	if isempty(singleSubFolder)
		break;
	end
	listOfFolderNames = [listOfFolderNames singleSubFolder];
end
numberOfFolders = length(listOfFolderNames) ;

p = 0;
% FileNumArray = [] ;
% Process all image files in those folders.
for k = 2 : numberOfFolders %Discard first foldes as it is the root
	% Get this folder and print it out.
	thisFolder = listOfFolderNames{k};
	fprintf('Processing folder %s\n', thisFolder);
	
	% List TIF files.
	filePattern = sprintf('%s/*.tif', thisFolder);
    files =  dir(filePattern);
    
    for j = 1 : size(files,1)
    info = imfinfo([thisFolder,'/',files(j).name]); %Extract information about the TIFF structure
    NumberStacks = numel(info); %Extract the number of frames which will give the iterations
    Tiff_stack = zeros([info(1).Width, info(1).Height, NumberStacks]); %Initializing variables for faster loading
    %disp(['Reading file: ', num2str(j)]);
    
    for i = 1 : NumberStacks
        Tiff_stack(:,:,i) = imread([thisFolder,'/',files(j).name], i);     
    end
    
    % Save Tiff_Stack as .em
    emwrite(Tiff_stack , [output_path,'/','par_', num2str(p+j),'.em'] )
    disp(['File: ',output_path,'par_', num2str(p+j),'.em  has been created']);
    end
	p = p+j;		
    disp(['File: ', num2str(k),' had ', num2str(p), ' particles']);
    
    FileNumArray = [FileNumArray, k-1 ]; %array that contains the file number of each particle
end


%% MOTL
% 2. Create Motive List based on particle centers

mkdir motl;
filesTXT = dir('*.txt'); 
M = dlmread(filesTXT(1).name, ' ', 1, 0); %Avoind first row & initialize with first file
FileCount = zeros( length(M),1 );
FileCount(:) = 1;
disp ( ['Motl had originally '  num2str(length(M)) ' particles'] ) ;

if size(filesTXT,1) > 1
    
    disp (' more than a single file was detected' ) ;
    
    for i = 2: size(filesTXT,1)
   
    % Original format text: ID X Y Z
    N = dlmread(filesTXT(i).name, ' ', 1, 0); %Avoind first row 

    % Change internal ID to the consecutive number of particles
    N(:,1) = ( length(M) + 1 : length(M) + length(N) )  ;

    % concatenate subsequent file to original file C = [A; B].
    M = [ M ; N ] ;
    
    FileCount2 = zeros( length(N),1 );
    FileCount2(:) = i;
    FileCount = [ FileCount ; FileCount2 ] ;
    disp ( ['end of loop '  num2str(i)] ) ;
    
    end
    
else
    
    Motl = zeros(20,length(M)); % Create Motl structure (20 rows per particle)
    M(:,1) = [1:size(M,1)] ;
    M = M' ;
    Motl(4,:) = M(:,1) ; % Particle ID
    Motl(5,:) = 1 ; % File Number
    Motl(8,:) = M(:,2) ; % X - Coordinate 
    Motl(9,:) = M(:,3) ; % Y - Coordinate 
    Motl(10,:) = M(:,4) ; % Z - Coordinate 
   
    emwrite(Motl,'motl/SingleFile_TPR-mEGFP_motl_1.em');
    
end

disp ( ['Motl has '  num2str(length(M)) ' particles'] ) ;

Motl = zeros(20,length(M)); % Create Motl structure (20 rows per particle)
M(:,1) = [1:size(M,1)] ;
M = M' ;
FileCount = FileCount' ;
Motl(4,:) = M(1,:) ; % Particle ID
Motl(5,:) = FileCount ; % File Number
Motl(8,:) = M(2,:) ; % X - Coordinate 
Motl(9,:) = M(3,:) ; % Y - Coordinate 
Motl(10,:) = M(4,:) ; % Z - Coordinate 
   
emwrite(Motl,'motl/SNAP-RanBP2_A647_motl_1.em');


%% UNIQUE IDENTIFIER
% Create unique identifier for extracted particles 
% Saves: File Name/Original ParticleID/XYZ coordinates/New ParticleID
% VJS 11.07.17 

clear all;
filesTXT = dir('*.txt'); 
M = dlmread(filesTXT(1).name, ' ', 1, 0); %Avoind first row & initialize with first file

FileCountArray = cell( length(M),1 ); %Initializing Cell where the file name will be saved
FileCountArray(:) = java.lang.String(filesTXT(1).name); %Saving first File Name
disp ( ['Motl had originally '  num2str(length(M)) ' particles'] ) ;

    
for i = 2: size(filesTXT,1)
   
    % Original format text: ID X Y Z
    N = dlmread(filesTXT(i).name, ' ', 1, 0); %Avoind first row 

    % concatenate subsequent file to original file C = [A; B].
    M = [ M ; N ] ;

    FileCountArray2 = cell( length(N),1 );
    FileCountArray2(:) = java.lang.String(filesTXT(2).name);
    FileCountArray = [ FileCountArray ; FileCountArray2 ] ;

    disp ( ['end of loop '  num2str(i)] ) ;
    
 end
    
length(M)
disp ( ['Motl has '  num2str(length(M)) ' particles'] ) ;

UniqueID = cell(6,length(M)); %Cell structure to save FileName together with Particle ID and XYZ coodrinnates
UniqueID = UniqueID' ;
UniqueID(:,1) = FileCountArray ; % FileName
ArrayM = num2cell(M) ;
UniqueID(:,2) = ArrayM(:,1) ; % Original Particle ID
UniqueID(:,3) = ArrayM(:,2) ;  % X - Coordinate
UniqueID(:,4) = ArrayM(:,3) ; % Y - Coordinate 
UniqueID(:,5) = ArrayM(:,4) ; % Z - Coordinate 
UniqueID(:,6) = num2cell([1:size(ArrayM,1)]) ; % New Particle ID

save ('UniqueIdentifier.mat' , 'UniqueID') ;

%% REFERENCE
% Generating the initial Reference -> alternatively use reference.m
% files emread.m and tom_emwrite need to be in the path
% required file in path emread.m 

clear all;
mkdir('ref');
cd EMpar
files = dir ('par*.em');

average = zeros (40,40,40);

for i = 1 : size (files,1);
    par = emread(files(i).name);
    tom_dev(par)
    average = average + par;
end

tom_emwrite('../ref/mEGFP-RanBP2_class5_ref_1.em',average)

cd ..

%% CREATE LIST FOR MISSING WEDGE
% Create ta_list.txt from tomo_list.txt

fid = fopen ('tomo_list.txt','W');
files = dir('*.txt');
for i = 1: size (files,1) 
    fprintf(fid,[files(i).name '\n']); % drag and drop filename into tomo list
end
fclose(fid);

fid = fopen ('ta_list.txt','W');
files = dir('*.txt');
for i = 1: size (files,1) 
    fprintf(fid,[files(i).name ' [-90.0 90.0]\n']); % drag and drop filename into tomo list and add tilt angles
end
fclose(fid);

%% CREATE MISSING WEDGE
% Create Missing Wedge  -> Alternatively use taList2wedge.m
% Even though we do not have a missing wedge in our data, it is a parameter
% we will need to incorporate later in TOM's 3D averaging function (av3_scan_angles_exact)

listFile = 'tomo_list.txt';
taList = 'ta_list.txt';
wedgeFile = 'wedge.em';
pixelSize = 7; %nm
binFactor = 1; 

[tomoList, cornerList] = parse_tomo_list2(listFile);
taList = parse_ta_list(taList);

% Setup wedgelist (TO BE REVISED)
wedgeList = zeros(5, numel(tomoList));
wedgeList(1, :) = 1:numel(tomoList); % tomogram number
wedgeList(4, :) = ones(1, numel(tomoList))*pixelSize*10; % pixel size
wedgeList(5, :) = ones(1, numel(tomoList))*(binFactor - 1);

for i = 1:numel(tomoList)	
	% Wedge
	wedgeList(2:3, i) = taList{i,2}'; % check
end

emwrite(wedgeList, wedgeFile);

%% SINGLE PARTICLE AVERAGING
% 3D Single Particle Averaging by Cross-correlation

% Load the mask and wedge you would like to use
% For no mask -> use storm_mask.em
% For Nuclear mask -> use storm_mask_nr.em
% For Cytoplasmic mask -> use storm_mask_cr.em

mask = tom_emread('storm_mask.em');
mask =mask.Value;
wedge = tom_emread('wedge.em');
wedge = wedge.Value;

% To randomnize particles
motl= emread('motl/SNAP-Nup133_class4_Random_motl_1.em');
for i=size(motl,2)
    motl(17,i)=motl(17,i)+floor(rand*8)*45; 
end

av3_scan_angles_exact('ref/SNAP-Nup133_class1_Random_ref','motl/SNAP-Nup133_class1_Random_motl','EMpar/par',1,10,3,3,mask,25,1,1,0,wedge,0);

% To forces 8-fold symetry for example use:

av3_scan_angles_exact('ref/SNAP-Seh1_class1_8fold_ref','motl/SNAP-Seh1_class1_8fold_motl','EMpar/par',1,10,3,3,mask,25,1,8,0,wedge,0);

%%
% To visualize your .em data or extract information from it, there are
% many usefull functions already implemeted on the TOM Toolbox -> see http://wwwuser.gwdg.de/~tomtool/
% Also download Chimera to see the density maps and make figures -> see https://www.cgl.ucsf.edu/chimera/download.html
% Some examples:

% To visualize your 3D particles in a gallery
ref=tom_emread('FileName.em');
figure; tom_dspcub(ref.Value); % Top View
figure; tom_dspcub(ref.Value,1); % Side View

% To calculate mean, max, min, standard-deviation, variance of an image
ref=tom_emread('FileName.em');
[a,b,c,d,e]=tom_dev(ref.Value,suppress);


