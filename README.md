# Analysis of the Architecture of the Nuclear Pore Complex by 3D super-resolution fluorescence microscopy

PhD dissertation, Vilma Jimenez Sabinina. Defence on Novemeber 27th, 2018.:mortar_board:

:email: Contact: Vilma Jimenez Sabinina (@sabinina, vilma.jimenez@embl.de and jimenez.sabinina@gmail.com)



Summary:

Nuclear pore complexes (NPCs) are the unique gateway that mediates all the traffic between the nucleus and the cytoplasm. In higher eukaryotes, each NPC is composed of multiple copies of approximately 30 different proteins termed nucleoporins and has a mass of over 100 MDa. 
In recent years, substantial effort has been devoted to the structural and functional characterization of this essential molecular machine in eukaryotic cells. Even though a pseudo-atomic model of the scaffold of the NPC has been accomplished, many details of the structure still remain elusive due to the enormous size, complexity and conformational dynamics of the NPC. In addition, we have little structural understanding of how such a large machine is assembled and what dynamic structural changes underlie its functions.
During my PhD work, I established a methodology that can determine the 3D architecture of the NPC by combining single-molecule localization microscopy in a 4Pi detection scheme with computational classification and 3D single particle averaging, which can resolve the structure of the NPC with molecular specificity and nano-scale resolution in situ in human cells.
My results present the reconstruction of a 3D molecular map that integrates both scaffold and flexible nucleoporins and that allow us to address dynamic parts of the NPC that have been inaccessible for atomic resolution methods. My findings indicate that the peripheral regions of the NPC can assume very different conformational states and that even the overall scaffold structure of the NPC is more flexible than previously thought. The methodology I have established opens the exciting possibility to address novel structural, functional and assembly aspects of this fundamental cellular machine and can be applied to interrogate the 3D architecture of other large protein complexes and organelles inside cells.
