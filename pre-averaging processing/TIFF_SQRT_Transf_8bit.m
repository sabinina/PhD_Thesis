clear variables;
files = dir('*.tif'); 

for k = 1 : size(files,1)
    
    info = imfinfo(files(k).name); %Extract information about the TIFF structure
    NumberStacks = numel(info); %Extract the number of frames which will give the iterations
    Tiff_stack = zeros([info(k).Height,info(k).Width, NumberStacks]); %Initializing variables for faster loading
    %Tiff_stack = zeros([info(k).Width, info(k).Height, NumberStacks]); %Initializing variables for faster loading
    %Sqr_Tiff = zeros([info(1).Width, info(1).Height, NumberStacks]); %Initializing variables for faster loading

    for i = 1 : NumberStacks
            disp(['Reading frame: ', num2str(i)]);
            Tiff_stack(:,:,i) = imread(files(k).name, i);     
    end

    %% Localization distribution of the image
    
    %figure; hist(Tiff_stack(Tiff_stack(:) > 0)); title(['Inital Localization Distribution: File ' num2str(k)]);
    
    % SQRT Transformation of Localization distribution
    Tiff_stack = double(Tiff_stack); % Use when working with UNIT8 datatype
    SQRT_Tiff_stack = sqrt(Tiff_stack);
    SQRT_Tiff_stack = SQRT_Tiff_stack .* ( max(Tiff_stack(:)) / max(SQRT_Tiff_stack(:)) );

    %figure; hist(SQRT_Tiff_stack(SQRT_Tiff_stack(:) > 0)); title(['SQRT Localization Distribution: File ' num2str(k)]);
    
    filename = ['SQRT_Norm_8bit_' files(k).name] ;
    imwrite(uint8(SQRT_Tiff_stack(:,:,1)), filename, 'tiff')
    for j = 2:size(SQRT_Tiff_stack,3)
        %imwrite(SQRT_Tiff_stack(:,:,j),filename, 'writemode', 'append');
        imwrite(uint8(SQRT_Tiff_stack(:,:,j)),filename, 'tiff','writemode', 'append');
    end
 
 
end