# Author: jkh1
## 2017-11-27

## Extracts x,y,z coordinates of nuclear pores from multipage TIFF file output of super-resolution microscopy experiment.
## Identifies bounding box of individual nuclear pores from one colour channel and extracts it from both colour channels.

library(optparse)
library(EBImage)
library(rTensor)
library(plotrix)
library(HoughTransform) # Custom package, i.e. not on CRAN


option_list = list(
  make_option(c("-i", "--channel1"), action = "store", default = NA, type='character',
              help="Path to multipage TIFF file for first channel to use as input"),
  make_option(c("-j", "--channel2"), action = "store", default = NA, type='character',
              help="Path to multipage TIFF file for second channel to use as input"),
  make_option(c("-b", "--box_size"), action = "store", default = "40:40:40", type = "character", help = "Size of the bounding box to extract in the form size along x:size along y:size along z (default=%default)"),
  make_option(c("-r", "--range"), action = "store", default = NA, type = "character", help = "Radius range in the form rmin:rmax for the circle Hough transform."),
  make_option(c("-s", "--score"), action = "store", default = 0.5, type = "numeric", help = "Score threshold for the circle Hough transform (default=%default)."),
  make_option(c("--serialize"), action = "store", default = "on", type = "character",
              help="Save serialized input data tensor to an .rds file (default=%default). Set this option to off to turn off."),
  make_option(c("--visualize"), action = "store", default = "on", type = "character",
              help="Draw detected circles on z-projection and save as PNG image (default=%default). Set this option to off to turn off.")
)


opt_parser <- OptionParser(option_list=option_list)
opt = parse_args(opt_parser)

if (is.na(opt$channel1)){
  print_help(opt_parser)
  stop("Argument --channel1 required.", call.=FALSE)
}
if (!file.exists(opt$channel1)){
  stop("\nERROR: Input file for channel 1 not found.", call.=FALSE)
}
if (is.na(opt$channel2)){
  print_help(opt_parser)
  stop("Argument --channel2 required.", call.=FALSE)
}
if (!file.exists(opt$channel2)){
  stop("\nERROR: Input file for channel 2 not found.", call.=FALSE)
}
if (is.na(opt$range)) {
  print_help(opt_parser)
  stop("Argument --range required for circle Hough transform.", call.=FALSE)
}
radii <- as.numeric(strsplit(opt$range,":")[[1]])
if (is.na(radii[1]) || radii[2]<radii[1]) {
  print_help(opt_parser)
  stop("Value for --range argument is invalid.", call.=FALSE)
}

## Read each 3D image into a tensor 
## This can take some time (and memory) so we do it once
## then serialize the tensor object for future use
write("Reading images...", stderr())

fileList <- list(opt$channel1, opt$channel2)
TList <- list()
imageSizes <- list()
imageNames <- list()
for(i in 1:length(fileList)) {
  imageFile <- basename(fileList[[i]])
  imagedir <- dirname(fileList[[i]])
  
  ## Use serialized tensor if it exists
  imageNames[[i]] <- sub(".tif","",imageFile)
  tensorfile <- paste0(imagedir,"/",imageNames[[i]],".tensor.rds")
  if(!file.exists(tensorfile) || opt$serialize == "off"){ # No serialized tensor, read image file
    TList[[i]] <- as.tensor(as.array(readImage(paste(imagedir,"/",imageFile,sep=""))))
    if(opt$serialize != "off") {
      saveRDS(TList[[i]],tensorfile)
    }
  } else { # Read serialized tensor
    TList[[i]] <- readRDS(tensorfile)
  }
  invisible(gc()) # Suppress output
  
  # Filter out slices with NAs
  z.with.na <- unique(which(is.na(TList[[i]]@data), arr.ind = TRUE)[,3])
  if (length(z.with.na)>0) {
    message("Found Z slices with NAs. Discarding them.")
    TList[[i]] <- TList[[i]][,,-z.with.na] 
  }
  imageSizes[[i]] <- dim(TList[[i]])
}
write("Done.\n", stderr())

if (imageNames[[1]] != imageNames[[2]]) {
  warning(paste0("File names for channel 1 and 2 differ. Output will be identified using the name for channel 2: ", imageNames[[2]]))
}

write("Applying Hough transform to channel 1 ...", stderr())

## 2D projection
T2D <- modeSum(TList[[1]], 3, drop = TRUE)
img <- T2D@data

## Function to convert index in a dist object to row,column coordinates of the corresponding distance matrix
rowcol <- function(idx,n) { # n is the size of the distance matrix
  nr <- ceiling(n-(1+sqrt(1+4*(n^2-n-2*idx)))/2)
  nc <- n-(2*n-nr+1)*nr/2+idx+nr
  cbind(nr,nc)
}

circles <- circle_hough_transform(img, rmin = radii[1], rmax = radii[2], threshold = opt$score)


if(dim(circles)[1] == 0) {
  stop("No circles were found.\n", call.=FALSE)
}

## Filter out overlapping circles
d <- dist(circles[, 1:2], method = "euclidean")
idx <- which(d <= 2*radii[2], arr.ind = TRUE)
excluded <- unique(as.vector(rowcol(idx, dim(circles)[1])))
circles <- circles[-excluded, ]

if(dim(circles)[1] == 0) {
  stop("No remaining circles after excluding overlapping ones.\n", call.=FALSE)
}

#if(opt$visualize != "off") {
#  ## Draw circles on the image
#  png(paste0(imageNames[[1]],".circles.png"), width = dim(img)[1]*2, height = dim(img)[2]*2, unit = "px", res = 300)
#  image(1:dim(img)[1], 1:dim(img)[2], img, col = gray.colors(10, start = 0))
#  for(i in 1:dim(circles)[1]) {
#    draw.circle(circles[i,1], circles[i,2], circles[i,3], lwd = 1, border = "cyan")
#  }
#  dev.off()
#}

## Find z coordinate of each circle
locations <- data.frame(id = numeric(), x = numeric(), y = numeric(), z = numeric())
for(i in 1:dim(circles)[1]) {
  xy.centre <- circles[i, 1:2]
  # Bounding box corners
  x.min = xy.centre$x - radii[2]
  x.max = xy.centre$x + radii[2]
  y.min = xy.centre$y - radii[2]
  y.max = xy.centre$y + radii[2]
  
  if (x.min < 1 || x.max > imageSizes[[1]][1] || y.min < 1 || y.max > imageSizes[[1]][2]) {
    next
  }
  
  box <- TList[[1]][x.min:x.max, y.min:y.max, ]
  
  ## Project on z axis
  Tz <- modeSum(box, 1, drop = TRUE)
  Tz <- as.vector(modeSum(Tz, 1, drop = TRUE)@data)
  
  ## z position is the centre of mass
  z <- round(weighted.mean(1:length(Tz), Tz))
  
  locations[i,] <- c(i, xy.centre$x, xy.centre$y, z)
}
write("Done.\n", stderr())
write(paste("Selected pores: ", dim(locations)[1], sep = ""), stderr())

write("Extracting nuclear pores...\n", stderr())
## Extract individual nuclear pores
## Create output directory if it doesn't exist
suppressWarnings(dir.create(file.path("extracted_pores")))
suppressWarnings(dir.create(file.path("extracted_pores", imageNames[[2]])))
suppressWarnings(dir.create(file.path("extracted_pores", imageNames[[2]], "channel1")))
suppressWarnings(dir.create(file.path("extracted_pores", imageNames[[2]], "channel2")))

## Bounding box size
pore.size <- as.numeric(strsplit(opt$box,":")[[1]])

count <- 1
skipped <- 0

locationsNew <- data.frame(id = numeric(), x = numeric(), y = numeric(), z = numeric())

for(i in 1:dim(locations)[1]) {
  
  if(is.na(locations[i, "z"])) {
    skipped <- skipped + 1
    next
  }
  
  centre <- locations[i,]
  
  image <- array(0, dim = pore.size)
  
  # Bounding box corners
  x.min = centre$x - floor(pore.size[1]/2)
  x.max = centre$x + floor(pore.size[1]/2)-1
  y.min = centre$y - floor(pore.size[2]/2)
  y.max = centre$y + floor(pore.size[2]/2)-1
  z.min = centre$z - floor(pore.size[3]/2)
  z.max = centre$z + floor(pore.size[3]/2)-1
  
  # Discard if we're close to an edge of the original image
  # because structure is likely incomplete
  if (x.min < 1 || x.max > imageSizes[[1]][1] || y.min < 1 || y.max > imageSizes[[1]][2] || z.min < 1 || z.max > imageSizes[[1]][3]) {
    skipped <- skipped + 1
    next
  }
  
  ch2.image <- TList[[2]][x.min:x.max, y.min:y.max, z.min:z.max]@data
  if (all(ch2.image == 0)) {
    # Extracted image is empty
    next
  }
  ch1.image <- TList[[1]][x.min:x.max, y.min:y.max, z.min:z.max]@data
  
  fileName <- paste0(file.path("extracted_pores", imageNames[[2]], "channel1"), '/pore_', count, '.tif')
  writeImage(ch1.image, fileName, type = "tiff", bits.per.sample = 8L, compression = "LZW")
  fileName <- paste0(file.path("extracted_pores", imageNames[[2]], "channel2"), '/pore_', count, '.tif')
  writeImage(ch2.image, fileName, type = "tiff", bits.per.sample = 8L, compression = "LZW")
  
  locationsNew <- rbind(locationsNew,centre)
  count <- count + 1
  
}

outputTable <- paste0(imageNames[[2]],".coordinates.txt")

write.table(locationsNew, file = outputTable, row.names=FALSE)

write("Done.\n", stderr())
write(paste0("Skipped ",skipped," clusters close to image edge.\n"), stderr())
write(paste("Saved : ", dim(locationsNew)[1], sep = ""), stderr())
